package week3.day4;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class Practise {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		List<Integer> list = new ArrayList<Integer>();
		
		list.add(11);
		list.add(12);
		list.add(13);
		list.add(14);
		list.add(15);
		
		calculate(list, new Predicate<Integer> () {
			public boolean test(Integer t)
			{
				return t%2 == 0;
			}
		}
				);
		
		calculate(list, (t) -> {return t%2 ==0;}
		);
		
		calculate(list, (t) -> {return t%2 !=0;}
				);
		
		
		List<String> names = new ArrayList<String>();
		
		
		names.add("Shalini");
		names.add("Shristi");
		names.add("Gokul");
		
		calculateLength(names, (t) -> t.length());	
		
		names.forEach(r -> System.out.println(r.toUpperCase()));
		
	}
	
	public static void calculateLength( List<String> nos, Function<String, Integer> function)
	{
		for(String n: nos)
		{
			System.out.print(function.apply(n));
		}
	}
	public static void calculate(List<Integer> list, Predicate<Integer> predicate)
	{
		for(Integer n:list)
		{
			if(predicate.test(n))
				System.out.println(n);
		}
	};
}



