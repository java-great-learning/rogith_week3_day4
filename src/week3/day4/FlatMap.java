package week3.day4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FlatMap {
	public static void main(String[] args)
	{
		
	List<List<Integer>> numbers = new ArrayList<List<Integer>>();
	numbers.add(Arrays.asList(1,2,3,4,5));
	numbers.add(Arrays.asList(10,20,30,40,50));
	numbers.add(Arrays.asList(11,21,31,41,51));
	
	for(List<Integer> list: numbers)
	{
		for(int x: list)
		{
			System.out.println(x);
		}
	}
	
	numbers.stream().flatMap(list -> list.stream()).forEach(System.out::println);
	
	}
}
