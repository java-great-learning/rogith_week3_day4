package week3.day4;

import java.util.ArrayList;
import java.util.List;

public class Lab2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		List<EmployeeDetails> employees = new ArrayList<EmployeeDetails>();

		employees.add(new EmployeeDetails(1,"Shalini",10000,"Development"));
		employees.add(new EmployeeDetails(2,"Rogith",100000,"HR"));
		employees.add(new EmployeeDetails(3,"Shristi",20000,"Deveopment"));
		employees.add(new EmployeeDetails(4,"Sunil",30000,"HR"));
		employees.add(new EmployeeDetails(5,"Sunillll",400000,"Development"));
		employees.add(new EmployeeDetails(6,"Suniiiiiiil",60000,"Development"));
		
		System.out.println(employees.stream().filter(t-> t.getDepartment().equals("Development")).mapToInt(s-> s.getSalary()).average());
	}

}
