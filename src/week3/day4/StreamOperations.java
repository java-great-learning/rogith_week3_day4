package week3.day4;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class StreamOperations {

	public static void main(String args[]) {
		List<String> names = new ArrayList<String>();


		names.add("Shalini");
		names.add("Shristi");
		names.add("Gokul");
		names.add("Aravindh");
		names.add("Uber");

		Stream<String> nameString = names.stream();

		Stream<String> converted = nameString.map(s ->{
			System.out.println("map "+s);
			return s+"Welcome Home";
		});

		names.stream().map(name -> "Wish you good luck"+ name).forEach(System.out::println);
		converted.forEach(System.out::println);

		List<Employee> employees = new ArrayList<Employee>();

		employees.add(new Employee(1,"Shalini","Mumbai"));
		employees.add(new Employee(2,"Rogith","Mumbai"));
		employees.add(new Employee(3,"Shristi","Chennai"));
		employees.add(new Employee(4,"Sunil","Goa"));
		employees.add(new Employee(5,"Sunillll","Bangalore"));
		employees.add(new Employee(6,"Suniiiiiiil","Goa"));

		employees.stream().map(name -> name.getEname().toUpperCase()).forEach(System.out::println);

		employees.stream().filter(city -> city.getCity().equals("Mumbai")).forEach(System.out::println);

	}
}
