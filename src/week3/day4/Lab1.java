package week3.day4;

import java.util.ArrayList;
import java.util.List;

public class Lab1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		List<Integer> list = new ArrayList<Integer>();
		
		list.add(11);
		list.add(12);
		list.add(13);
		list.add(15);
		list.add(14);
		list.add(16);
		list.add(17);
		
		System.out.println(list.stream().filter(t-> t%2  != 0).mapToInt(s-> s*s).max().getAsInt());
	}

}
